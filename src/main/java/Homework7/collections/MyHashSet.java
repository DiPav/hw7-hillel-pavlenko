package Homework7.collections;

import java.util.*;

public class MyHashSet<T> implements Set<T> {

    private static final int DEFAULT_CAPACITY = 16;

    private static class Node {
        Object value;
        Node next;

        public Node(Object value) {
            this.value = value;
        }

        boolean hasValue(Object value) {
            return this.value.equals(value);
        }
    }

    private Node[] nodes;
    private int size = 0;
    private int capacity;

    public MyHashSet() {
        this(DEFAULT_CAPACITY);
    }

    public MyHashSet(int capacity) {
        this.capacity = capacity;
        this.nodes = new Node[capacity];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o == null) return false;
        int index = getIndex(o);
        for (Node current = nodes[index]; current != null; current = current.next) {
            if (current.hasValue(o)) {
                return true;
            }
        }
        return false;
    }

    private int getIndex(Object o) {
        return Math.abs(o.hashCode() % nodes.length);
    }

    @Override
    public Iterator<T> iterator() {
        return new MyHashSetIterator();
    }

    @Override
    public Object[] toArray() {
        Object[] newArray = new Object[size];
        int i = 0;
        for (Object elem : this) {
            newArray[i++] = elem;
        }
        return newArray;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        T1[] result = Arrays.copyOf(a, size);
        int i = 0;
        for (T elem : this) {
            result[i++] = (T1) elem;
        }
        return result;
    }

    @Override
    public boolean add(T value) {
        if (value == null) return false;

        Node node = new Node(value);
        int index = getIndex(value);

        if (nodes[index] == null) {
            nodes[index] = node;
            size++;
            return true;
        }

        for (Node current = nodes[index]; current != null; current = current.next) {
            if (current.hasValue(value)) {
                current.value = value;
                return false;
            } else if (current.next == null) {
                current.next = node;
                size++;
                break;
            }
        }
        return true;
    }

    @Override
    public boolean remove(Object o) {
        int index = getIndex(o);
        Node previous = null;
        for (Node current = nodes[index]; current != null; previous = current, current = current.next) {
            if (current.hasValue(o)) {
                size--;
                if (current == nodes[index]) {
                    nodes[index] = current.next;
                } else {
                    previous.next = current.next;
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!this.contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (Object o : c) {
            Node node = new Node(o);
            add((T) node.value);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (Object element : this) {
            if (!c.contains(element)) {
                remove(element);
            }
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object o : c) {
            remove(o);
        }
        return true;
    }

    @Override
    public void clear() {
        this.nodes = new Node[capacity];
        size = 0;
    }

    private class MyHashSetIterator implements Iterator<T> {
        int index = -1;
        int count = 0;
        Node current = null;

        public MyHashSetIterator() {
            this.current = nodes[0];
        }

        @Override
        public boolean hasNext() {
            return count < size;
        }

        private void findNext() {
            if (current != null && current.next != null) {
                current = current.next;
            } else {
                do {
                    current = nodes[++index];
                } while (current == null && index < nodes.length - 1);
            }
        }

        @Override
        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            findNext();
            count++;
            return (T) current.value;
        }
    }
}

package Homework7.collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class MyHashSetTest {

    MyHashSet<String> hashSet = new MyHashSet<>();

    @BeforeEach
    void setup() {
        hashSet.add("Ant");
        hashSet.add("Bear");
        hashSet.add("Cat");
        hashSet.add("Deer");
        hashSet.add("Eagle");
        hashSet.add("Fly");
    }

    @Test
    void shouldGiveSize() {
        assertEquals(hashSet.size(), 6);
    }

    @Test
    void shouldRemoveByValue() {
        assertEquals(hashSet.size(), 6);
        hashSet.remove("Ant");
        assertEquals(hashSet.size(), 5);
    }

    @Test
    void shouldFindByValue() {
        assertTrue(hashSet.contains("Bear"));
        assertFalse(hashSet.contains("Moose"));
    }

    @Test
    void shouldFindByOtherList() {
        MyHashSet<String> hashSet2 = new MyHashSet<>();
        hashSet2.add("Ant");
        hashSet2.add("Bear");
        hashSet2.add("Cat");
        hashSet2.add("Deer");
        hashSet2.add("Eagle");
        hashSet2.add("Fly");

        assertTrue(hashSet.containsAll(hashSet2));

        hashSet2.add("Moose");

        assertFalse(hashSet.containsAll(hashSet2));
    }

    @Test
    void shouldCheckEmptiness() {
        assertFalse(hashSet.isEmpty());
    }

    @Test
    void shouldGiveString() {
        assertEquals(Arrays.toString(hashSet.toArray()), "[Fly, Bear, Eagle, Cat, Ant, Deer]");
    }

    @Test
    void shouldBeCorrectRemoveAllMethod() {
        MyHashSet<String> hashSetList = new MyHashSet<>();
        hashSetList.add("Ant");
        hashSetList.add("Bear");
        hashSetList.add("Cat");
        hashSetList.add("Deer");

        assertTrue(hashSet.removeAll(hashSetList));

        assertEquals(Arrays.toString(hashSet.toArray()), "[Fly, Eagle]");
    }

    @Test
    void shouldClear() {
        hashSet.clear();

        assertEquals(hashSet.size(), 0);
    }

    @Test
    void shouldRetainAllByOtherList() {
        MyHashSet<String> hashSet2 = new MyHashSet<>();
        hashSet2.add("Ant");
        hashSet2.add("Bear");
        hashSet2.add("Cat");
        hashSet2.add("Deer");

        assertTrue(hashSet.retainAll(hashSet2));

        assertEquals(Arrays.toString(hashSet.toArray()), "[Bear, Cat, Ant, Deer]");
    }
}
